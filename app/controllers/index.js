$.index.open();


/* Import Canvas module */
if (OS_ANDROID) 
	var Canvas = require('com.wwl.canvas');
	
if (OS_IOS)
	var Canvas = require('ti.canvas');
	
Ti.App.currentPos = 0;

var useekProgress = {
	backgroundColor: 'transparent',
	top: 0,
	left: 0,
	radius: 80,
	lineWidth: 5,
	strokeStyle: 'silver', // white is not working, wtf??
    shadowColor: 'cyan', // TiCanvasView (ios) only uses named colors
    shadowBlur: 10,
    shadowOffsetX: 0,
    shadowOffsetY: 0,
    startX: 0,
    startY: 0,
    width: 0,
    height: 0,
    currentPos: 0,
	init: function(){
		this.width = this.radius * 2;
		this.height = this.radius * 2;
		if (OS_ANDROID){
			this.startX =  (this.shadowBlur/2) + this.lineWidth;
			this.startY =  (this.shadowBlur/2) + this.lineWidth;
			if (this.shadowOffsetX < 0 ) {
				this.startX = this.startX - this.shadowOffsetX;
			};
			if (this.shadowOffsetY < 0 ) {
				this.startY = this.startY - this.shadowOffsetY;
			};
			this.canvas = Canvas.createCanvasView({
				top: this.top,
				left: this.left,
				width: this.width + this.shadowBlur + Math.abs(this.shadowOffsetX),
				height: this.height + this.shadowBlur + Math.abs(this.shadowOffsetY),
			    backgroundColor: this.backgroundColor
			});
		}
		if (OS_IOS) {
			this.startX = this.radius + this.lineWidth;
			this.startY = this.radius + this.lineWidth;
			if (this.shadowOffsetX < 0 ) {
				this.startX = this.startX - this.shadowOffsetX/2;
			};
			if (this.shadowOffsetY < 0 ) {
				this.startY = this.startY - this.shadowOffsetY/2;
			};
			this.canvas = Canvas.createView();
		}
	},
	draw: function(progress){ // float 0.0 to 1.0
		if (OS_ANDROID){
			this.canvas.clear();
			this.canvas.beginPath();
			this.canvas.lineWidth = this.lineWidth;
			this.canvas.strokeWidth = this.lineWidth;
			this.canvas.strokeStyle = this.strokeStyle;
		    this.canvas.shadowColor = this.shadowColor;
		    this.canvas.shadowBlur = this.shadowBlur;
		    this.canvas.shadowOffsetX = this.shadowOffsetX;
		    this.canvas.shadowOffsetY = this.shadowOffsetY;
			this.canvas.arc(this.startX, this.startY, this.width * 2, this.height * 2,0, 360*progress);
			this.canvas.stroke();
		}
		if (OS_IOS) {
			this.canvas.begin();
			this.canvas.lineWidth(this.lineWidth);
			this.canvas.strokeStyle(this.strokeStyle);
			this.canvas.shadow(0, 0, this.shadowBlur, this.shadowColor);
			
			this.canvas.arc(this.startX, this.startY, this.radius, (360*progress) * (Math.PI / 180), 0, 1); // Uses radians
			this.canvas.stroke();
			this.canvas.commit();
		}
	}
};

useekProgress.init();
$.currentProgress.text = 'Starting progress';

function updateProgress(){
	if (Ti.App.currentPos >= 100) {
		Ti.App.currentPos = 0;
	} 
	Ti.App.currentPos += 1;
	useekProgress.draw(Ti.App.currentPos/100);
	$.currentProgress.text = 'Current progress: ' + Ti.App.currentPos;
}

if (OS_ANDROID) {
	/* The canvas view fires a load event when it is ready to draw upon */ 
	useekProgress.canvas.addEventListener('load', function() {		
		Ti.App.progressInterval = setInterval(updateProgress, 250);		    
	});
}

if (OS_IOS) {
	Ti.App.progressInterval = setInterval(updateProgress, 250);		    
}



$.useekCanvas.add(useekProgress.canvas);